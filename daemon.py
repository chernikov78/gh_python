#!/usr/bin/env python3

import sys, os, time, atexit
import subprocess
from signal import SIGTERM

class Daemon:
#        """
#        A generic daemon class.
#
#        Usage: subclass the Daemon class and override the run() method
#        """
	def __init__(self, pidfile, stdin='/dev/null', stdout='/dev/null', stderr='/dev/null'):
		self.stdin = stdin
		self.stdout = stdout
		self.stderr = stderr
		self.pidfile = pidfile

	def daemonize(self):
#                """
#                do the UNIX double-fork magic, see Stevens' "Advanced
#                Programming in the UNIX Environment" for details (ISBN 0201563177)
#                http://www.erlenstar.demon.co.uk/unix/faq_2.html#SEC16
#                """
		try:
			pid = os.fork()
			if pid > 0:
                                # exit first parent
				sys.exit(0)
		except OSError as err:
			sys.stderr.write("fork 1 failed: %d (%s)\n" % (err.errno, err.strerror))
			sys.exit(1)

                # decouple from parent environment
		os.chdir("/")
		os.setsid()
		os.umask(0)

                # do second fork
		try:
			pid = os.fork()
			if pid > 0:
                                # exit from second parent
				sys.exit(0)
		except OSError as e:
			sys.stderr.write("fork 2 failed: %d (%s)\n" % (e.errno, e.strerror))
			sys.exit(1)

                # redirect standard file descriptors
		sys.stdout.flush()
		sys.stderr.flush()
#		import pdb
#		pdb.set_trace()
#		si = open(self.stdin, 'r')
#		so = open(self.stdout, 'a+')
#		se = open(self.stderr, 'a+')
#		os.dup2(si, open(sys.stdin,'r'))
#		os.dup2(so, open(sys.stdout,'a+'))
#		os.dup2(se, open(sys.stderr, 'a+'))

                # write pidfile
		atexit.register(self.delpid)
		pid = str(os.getpid())
		fd=open(self.pidfile, 'w')
		fd.write("%s\n" % pid)
		fd.close()

	def delpid(self):
		os.remove(self.pidfile)

	def start(self):
# Start the daemon
# Check for a pidfile to see if the daemon already runing
		try:
			fd = open(self.pidfile, 'r')
			pid = int(fd.readline().strip())
			fd.close()
		except IOError:
			pid = None
		if pid:
			if os.path.isdir('/proc/{}'.format(pid)):
				message = "pidfile %s is already exist. Daemon is running!\n"
				sys.stderr.write(message % self.pidfile)
				sys.exit(1)
			else:
				os.remove(self.pidfile)
# Start the daemon
		self.daemonize()
		self.run()

	def stop(self):
# Stop the daemon
# Get the pid from the pidfile
		try:
			pf = open(self.pidfile,'r')
			pid = int(pf.readline().strip())
			pf.close()
		except IOError:
			pid = None
		if not pid:
			sys.stderr.write("Daemon wasn`t running!\n")
			return
		cmd = "ps -eo pid,ppid |grep "+str(pid)
		df = subprocess.Popen(cmd, shell = True, stdout = subprocess.PIPE) 
		# stdout = subprocess.PIPE - говорить про те, що вивід програми перехоплюється
		df1 = (df.communicate()[0]).decode('utf-8').replace(' '+str(pid), '').split('\n')
		try:
			df1.pop(0)
			df1.pop()
			os.kill(pid, SIGTERM)
			time.sleep(0.1)
			for item in df1:
				os.kill(int(item), SIGTERM)
				time.sleep(0.1)
		except IndexError:
			pass
		if os.path.exists(self.pidfile):
			os.remove(self.pidfile)

	def restart(self):
# Restart the daemon
		self.stop()
		self.start()

	def run(self):
# You should override this method when you subclass Daemon. It will be called after the process has been
# daemonized by start() or restart().
		pass
