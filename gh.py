#!/usr/bin/env python3

from flask import Flask, Response, request, render_template
from gevent.pywsgi import WSGIServer
import json
import datetime
import conf
from daemon import Daemon
import sys

from db_sql import DbProvider
from sess import Sessions

app = Flask(__name__, static_folder='static', template_folder='templates')

dbprovider = DbProvider(conf.BASE, conf.USR_PWD)
sessions = Sessions(dbprovider)

@app.after_request
def apply_caching(response):
    response.headers['Access-Control-Allow-Origin']=conf.WHO_FROM
    response.headers['Access-Control-Allow-Credentials']='true'
    response.headers['Access-Control-Allow-Method'] = 'POST'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, x-requested-with, X-PINGOTHER, Accept, Content-Type'
    response.headers['Access-Control-Max-Age'] = '3600'
    return response

def json_serial(o):
    if type(o) is datetime.date or type(o) is datetime.datetime:
        return o.isoformat()

@app.route('/', methods=['GET'])
def incoming():
    resp = Response(render_template('index.html'))
    return resp

@app.route('/whoAmI', methods=['GET'])
def whoAmI():
    sid = request.cookies.get('session')
    if sessions.findSession(sid=sid):
        
    else:
        json_resp=json.dumps({'isLoggedIn':'false', 'user_id':''})
        resp = Response(json_resp, status=200, mimetype='application/json')
    return resp

class MyDaemon(Daemon):
    def run(self):
        #http_server = WSGIServer((conf.IP_ADDR_SRV, conf.PORT_SRV), app, certfile=conf.CERT_FILE, keyfile=conf.KEY_FILE)
        http_server = WSGIServer((conf.IP_ADDR_SRV, conf.PORT_SRV), app)
        http_server.serve_forever()

if __name__ == '__main__':
    myDaemon = MyDaemon(conf.PID_FILE)
    if len(sys.argv) ==2:
        if 'start'==sys.argv[1]:
            myDaemon.start()
        elif 'stop'==sys.argv[1]:
            myDaemon.stop()
        elif 'restart'==sys.argv[1]:
            myDaemon.restart()
        else:
            print("Unknown command")
            print("usage: %s start|stop|restart" % sys.argv[0])
            sys.exit(2)
        sys.exit(0)
    else:
        print("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)