from db_sql import DbProvider
import random
import string
import time
from datetime import timedelta
import hashlib

class Sessions:
    
    def __init__(self, dbprov):
        self.dbprovider = dbprov

    def openSession(self, user='', adm_user=''):
        if self.findSession(user=user, adm_user=adm_user):
            sid = self.findSid(user=user, adm_user=adm_user)
            self.alterSessionTime(sid)
        else:
            lettersAndDigits = string.ascii_letters + string.digits
            sid = ''.join(random.choice(lettersAndDigits) for i in range(32))
            self.writeSession(sid, user=user, adm_user=adm_user)
        return sid

    def alterSessionTime(self, sid):
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        query = "UPDATE sessions SET expired=:time WHERE id=:sid"
        cur = self.dbprovider.makeQuery(query, {'time':currTime, 'sid':sid})
        cur.close()
        return 

    def writeSession(self, sid, user='', adm_user=''):
        currTime = time.strftime('%Y-%m-%d %H:%M:%S')
        if user:
            user_id = self.getUserId(user)
            query = "INSERT INTO sessions (id, users_id, expired) VALUES (:sid, :users_id, :time)"
            props = {'sid':sid, 'users_id':user_id, 'time':currTime}
        elif adm_user:
            adm_user_id = self.getAdmUserId(adm_user)
            query = "INSERT INTO sessions (id, adm_usr_id, expired) VALUES (:sid, :adm_usr_id, :time)"
            props = {'sid':sid, 'adm_usr_id':adm_user_id, 'time':currTime}
        else:
            return
        cur = self.dbprovider.makeQuery(query, props)
        cur.close()
        return

    def delSession(self, sid):
        query = "DELETE FROM sessions WHERE id=:sid"
        cur = self.dbprovider.makeQuery(query, {'sid':sid})
        cur.close()
        return
        
    def findSid(self, user='', adm_user=''):
        if user:
            user_id = self.getUserId(user)
            query = "SELECT id FROM sessions WHERE users_id=:user"
            props = {'user':user_id}
        elif adm_user:
            adm_user_id = self.getAdmUserId(adm_user)
            query = "SELECT id FROM sessions WHERE adm_usr_id=:adm_user"
            props = {'adm_user':adm_user_id}
        else:
            return ''
        cur = self.dbprovider.makeQuery(query, props)
        sid = cur.fetchone()['id']
        cur.close()
        if not sid:
            sid=''
        return sid

    def findSession(self, sid='', user=''):
        if sid:
            query = "SELECT * FROM SESSIONS WHERE sid=:sid"
            props = {'sid':sid}
        elif user:
            user_id = self.getUserId(user)
            query = "SELECT * FROM SESSIONS WHERE user_id=:user"
            props = {'user':user_id}
        else: 
            return False
        cur = self.dbprovider.makeQuery(query, props)
        rows = cur.fetchall()
        cur.close()
        if len(rows):
            result = True
        else:
            result = False
        return result

    def getInfoSession(self, sid):
        query = "SELECT adm_usr_id, users_id FROM sessions WHERE id=:sid"
        cur = self.dbprovider.makeQuery(query, {'sid':sid})
        row = cur.fetchone()
        cur.close()
        resp={'userName':'', 'isLoggedIn':'NeedToLogIn', 'firstName':'', 'lastName':'', 'lang':''}
        if row['adm_usr_id']:
            query = "SELECT email, lang FROM adm_usr WHERE id=:adm_id"
            cur = self.dbprovider.makeQuery(query, {'adm_id':row['adm_usr_id']})
            row = cur.fetchone()
            cur.close()
            resp['isLoggedIn']='Adm'
            resp['userName']= row['email']
            resp['lang']= row['lang']
        elif row['users_id']:
            query = "SELECT id, email, first_name, last_name, lang, current_budget, rows_per_page FROM users WHERE id=:user_id"
            cur = self.dbprovider.makeQuery(query, {'user_id':row['users_id']})
            row = cur.fetchone()
            cur.close()
            resp['isLoggedIn']='User'
            resp['userName']= row['email']
            resp['firstName']= row['first_name']
            resp['lastName']= row['last_name']
            resp['lang']= row['lang']
            resp['user_id']= row['id']
            resp['email']= row['email']
            resp['currentBudget']= row['current_budget']
            resp['rowsPerPage']= row['rows_per_page']
        else:
            resp['isLoggedIn']='NeedToLogIn'
        self.alterSessionTime(sid)
        return resp

    def getUserId(self, user):
        query = "SELECT id FROM users WHERE email=:user"
        cur = self.dbprovider.makeQuery(query, {'user':user})
        id = cur.fetchone()['id']
        cur.close()
        return id

    def getAdmUserId(self, user):
        query = "SELECT id FROM adm_usr WHERE email=:user"
        cur = self.dbprovider.makeQuery(query, {'user':user})
        id = cur.fetchone()['id']
        cur.close()
        return id

    def ifAdmin(self, user, pwd):
        hash = hashlib.md5(pwd.encode()).hexdigest()
        query = "SELECT email, passwd, lang FROM adm_usr WHERE email=:user AND passwd=:pwd"
        cur = self.dbprovider.makeQuery(query, {'user':user, 'pwd':hash})
        rows = cur.fetchall()
        cur.close()
        if len(rows):
            result=rows[0]['lang']
        else:
            result = False
        return result

    def getCountOfSessions(self):
        query = "SELECT COUNT(*) AS adm, (SELECT COUNT(*) FROM sessions GROUP BY adm_usr_id HAVING adm_usr_id IS null) AS usr FROM sessions GROUP BY users_id HAVING users_id IS null"
        cur = self.dbprovider.makeQuery(query,{})
        rows = cur.fetchall()
        cur.close()
        if len(rows):
            resp={'admCount':rows[0]['adm'], 'userCount':rows[0]['usr']}
        else:
            resp={'admCount':'0', 'userCount':'0'}
        return resp