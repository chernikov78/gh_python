import sqlalchemy
import hashlib

class DbProvider:

	def __init__(self, base, user):
		self.conn_str = 'mysql://'+user+'@localhost/'+base+'?charset=utf8'
		self.engine = sqlalchemy.create_engine(self.conn_str)

	def makeQuery(self, sql, args):
		cur=self.engine.execute(sqlalchemy.text(sql), args)
		return cur

	def makeQueryWithTransaction(self, sql, args):
		with self.engine.begin() as connection:
			cur = connection.execute(sqlalchemy.text(sql), args)	
		return